// Websites accessed: 27/03/19
// Code derived from: https://stackoverflow.com/questions/20631666/java-socket-programming-conversation
// https://www.geeksforgeeks.org/socket-programming-in-java/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Client {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        Client client = new Client(); // declaring
        client.start();
    }

    /**
     * @throws Exception
     */
    public void start()
            throws Exception { // to catch for errors earlier in the program and to be handled by JVM

        BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in)); // enable message from server
        Socket socket = new Socket("127.0.0.1", 8096);
        PrintStream print = new PrintStream(socket.getOutputStream());  // enable message to server
        print.println("hi");

        InputStreamReader iReader = new InputStreamReader(socket.getInputStream());
        BufferedReader bMessage = new BufferedReader(iReader); // puts server message into buffer
        String serverMessage =  bMessage.readLine().trim(); // server message stored here; trim used to cut down extra spacings of server message
        System.out.println(serverMessage);

        while (true){
            serverMessage =  bMessage.readLine();
            System.out.println(serverMessage);

            if (serverMessage.equalsIgnoreCase("Waiting for connection")){
                String clientMessage = bReader.readLine().trim(); // read from stdin
                print.println(clientMessage); // send message/number to server
                if (clientMessage.equalsIgnoreCase("bye")){
                    break;
                }
                serverMessage =  bMessage.readLine().trim();
                System.out.println(serverMessage);
            } // end first if

        } // end while

    } // end start() method
}